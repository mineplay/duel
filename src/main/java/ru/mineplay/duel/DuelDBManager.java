package ru.mineplay.duel;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.TableUtils;
import org.bukkit.Bukkit;
import ru.mineplay.mineplayapi.db.DatabaseTableConfigUtil;
import ru.mineplay.mineplayapi.db.IDBProvider;
import ru.mineplay.mineplayapi.pojo.PersistentPlayerStats;

import java.sql.SQLException;
import java.util.UUID;

public class DuelDBManager {
	private static Dao<PersistentPlayerStats, UUID> persistentPlayerStats = null;

	public static ConnectionSource getConnectionSource() {
		return Bukkit.getServer().getServicesManager().getRegistration(IDBProvider.class).getProvider().getConnectionSource();
	}

	public static Dao<PersistentPlayerStats, UUID> getPersistentPlayerStats() throws SQLException {
		if (persistentPlayerStats == null) {
			DatabaseTableConfig<PersistentPlayerStats> config = DatabaseTableConfigUtil.fromClass(getConnectionSource(), PersistentPlayerStats.class);
			config.setTableName("Duel_player_stats");
			TableUtils.createTableIfNotExists(getConnectionSource(), config);
			persistentPlayerStats = DaoManager.createDao(getConnectionSource(), config);
		}

		return persistentPlayerStats;
	}

}
