package ru.mineplay.duel.games;

import org.bukkit.configuration.ConfigurationSection;
import ru.mineplay.duel.matches.DuelMatch;
import ru.mineplay.duel.matches.DuelMatchTeamNew;
import ru.mineplay.mineplayapi.bars.ScoreBoard;
import ru.mineplay.mineplayapi.game.MiniGameBase;
import ru.mineplay.mineplayapi.game.arena.Arena;
import ru.mineplay.mineplayapi.game.match.Match;
import ru.mineplay.mineplayapi.game.stat.BasicMiniGameStats;

import java.util.ArrayList;


public class DuelMiniGame extends MiniGameBase<DuelMiniGame> {
	public DuelMiniGame(ConfigurationSection config) {
		super(config);
	}

	@Override
	protected Match<DuelMiniGame> instantiateMatch(Arena<DuelMiniGame> arena) {
		if (arena.getConfig().getString("arenatype").equals("DuelTeam"))
			return new DuelMatchTeamNew(this, arena);
		/*else if (arena.getConfig().getString("arenatype").equals("DuelFour"))
			return new DuelMatchFour(this, arena);*/
		else
			return new DuelMatch(this, arena);
	}

	@Override
	protected void onMatchFinished(boolean b) {

	}

	@Override
	protected BasicMiniGameStats.Template createGlobalPlayerStatsTemplate() {
		return new BasicMiniGameStats.BasicTemplate();
	}

	@Override
	public String getName() {
		return "DuelGame";
	}

	@Override
	public ArrayList<String> allSpots() {
		ArrayList<String> spots = new ArrayList<>();
		spots.add("team_a");
		spots.add("team_a2 (\"DuelTeam\" or \"DuelFour\" mode)");
		spots.add("team_b");
		spots.add("team_b2  (\"DuelTeam\" or \"DuelFour\" mode)");
		spots.add("waiting_room");
		return spots;
	}

	@Override
	public void resetArena(String s) {
		getArenas().get(s).getCurrentMatch().getPlayers().forEach(ScoreBoard::removeScoreBoardFromPlayer);
	}
}
