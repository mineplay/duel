package ru.mineplay.duel;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault {
	public static double getMoney(Player paramPlayer) {
		return Duel.econ.getBalance(paramPlayer.getName());
	}

	public static void addMoney(Player paramPlayer, double paramDouble) {
		setMoney(paramPlayer, getMoney(paramPlayer) + paramDouble);
	}

	public static void setMoney(Player paramPlayer, double paramDouble) {
		Duel.econ.depositPlayer(paramPlayer.getName(), paramDouble);
	}

	public static void removeMoney(Player paramPlayer, double paramDouble) {
		Duel.econ.withdrawPlayer(paramPlayer.getName(), paramDouble);
	}

	public static boolean setupEconomy() {
		if (Duel.getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider localRegisteredServiceProvider = Duel.getPlugin()
				.getServer()
				.getServicesManager()
				.getRegistration(Economy.class);
		if (localRegisteredServiceProvider == null) {
			return false;
		}
		Duel.econ = (Economy) localRegisteredServiceProvider.getProvider();
		return Duel.econ != null;
	}
}
