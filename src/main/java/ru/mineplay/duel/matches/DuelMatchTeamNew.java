package ru.mineplay.duel.matches;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.mineplay.duel.Duel;
import ru.mineplay.duel.games.DuelMiniGame;
import ru.mineplay.duel.utils.BlockSelectAction;
import ru.mineplay.kits.KitWrapperProvider;
import ru.mineplay.mineplayapi.bars.ScoreBoard;
import ru.mineplay.mineplayapi.game.arena.Arena;
import ru.mineplay.mineplayapi.game.cooldown.CooldownManager;
import ru.mineplay.mineplayapi.game.match.CannotJoinException;
import ru.mineplay.mineplayapi.game.match.IMatchState;
import ru.mineplay.mineplayapi.game.match.MatchBase;
import ru.mineplay.mineplayapi.game.match.MatchRunState;
import ru.mineplay.mineplayapi.game.stat.BasicMiniGameStats;
import ru.mineplay.mineplayapi.game.stat.BasicMiniGameStats.Template;
import ru.mineplay.mineplayapi.game.team.ITeam;
import ru.mineplay.mineplayapi.game.team.UnknownTeam;
import ru.mineplay.mineplayapi.inventory.item.MiniGameInventoryPlayerItem;
import ru.mineplay.mineplayapi.invgui.GuiElementGroup;
import ru.mineplay.mineplayapi.invgui.InventoryGui;
import ru.mineplay.mineplayapi.invgui.StaticGuiElement;

import java.util.*;
import java.util.stream.Collectors;

import static ru.mineplay.mineplayapi.i18n.MPLocalizer.l6e;

public class DuelMatchTeamNew extends MatchBase<DuelMiniGame> {
	/**
	 * ~2x2~
	 */
	private HashMap<Player, MiniGameInventoryPlayerItem> selectedkits = new HashMap<>();
	public int winCoin = 50;
	public int lockKit = -1;
	public static final String[] blockSelectGuiLayout = new String[]{
			"bbbbggggg",
			"ggggggggg",
			"kkkkkkkkk"
	};


	public CooldownManager<DuelMatchTeamNew> finishCooldown = new CooldownManager<DuelMatchTeamNew>(3600) {
		@Override
		public boolean checkConditions() {
			return getRunState() == MatchRunState.RUNNING;
		}

		@Override
		public void execute() {
			if (getPlayers(DuelMatch.DuelTeam.A).size() == 0)
				DuelMatch.winTeam(getPlayers(DuelMatch.DuelTeam.B).stream().map(Player::getUniqueId).collect(Collectors.toList()), winCoin);
			if (getPlayers(DuelMatch.DuelTeam.B).size() == 0)
				DuelMatch.winTeam(getPlayers(DuelMatch.DuelTeam.A).stream().map(Player::getUniqueId).collect(Collectors.toList()), winCoin);
			if (getPlayers(DuelMatch.DuelTeam.A).size() > 0 && getPlayers(DuelMatch.DuelTeam.B).size() > 0) {
				List<UUID> list = getPlayers(DuelMatch.DuelTeam.A).stream().map(Player::getUniqueId).collect(Collectors.toList());
				list.addAll(getPlayers(DuelMatch.DuelTeam.B).stream().map(Player::getUniqueId).collect(Collectors.toList()));
				DuelMatch.failerPlayers(list);
			}
			stateTransition(MatchRunState.FINISHED);
		}
	}.setCooldownDisplayHandler(
			new CooldownManager.ActionBarCooldownDisplayHandler(this,
					20, "Игра окончится через %d секунд",
					ImmutableSet.of(DuelMatch.DuelTeam.A, DuelMatch.DuelTeam.B)), 20, -1);

	public CooldownManager<DuelMatchTeamNew> startCooldown = new CooldownManager<DuelMatchTeamNew>(100) {
		@Override
		public boolean checkConditions() {
			return getRunState() == MatchRunState.PREPARING && getPlayers().size() == 4;
		}

		@Override
		public void execute() {
			stateTransition(MatchRunState.RUNNING);
		}
	}.setCooldownDisplayHandler(new CooldownManager.TitleCooldownDisplayHandler(this,
			20, "Игра начнётся через %d секунд",
			ImmutableSet.of(DuelMatch.DuelTeam.A, DuelMatch.DuelTeam.B)), 20, 100);

	public ArrayList<ItemStack> selectableBlocks = Lists.newArrayList(
			new ItemStack(Material.REDSTONE_BLOCK),
			new ItemStack(Material.GLOWSTONE));

	public DuelMatchTeamNew(DuelMiniGame minigame, Arena<DuelMiniGame> arena) {
		super(minigame, arena);
		initConfig();
	}

	private void initConfig() {
		if (getArena().getConfig().contains("winCoin"))
			winCoin = getArena().getConfig().getInt("winCoin");
		if (getArena().getConfig().contains("lockkit"))
			lockKit = getArena().getConfig().getInt("lockkit");
	}
	@Override
	protected Template createTeamStatsTemplate() {
		return new BasicMiniGameStats.BasicTemplate();
	}

	@Override
	protected Template createPlayerStatsTemplate() {
		return new BasicMiniGameStats.BasicTemplate();
	}

	@Override
	public int getMaxPlayers() {
		return 4;
	}
	@Override
	public void joinMatch(Player player) throws CannotJoinException {
		if (getRunState().getBaseState() != MatchRunState.PREPARING)
			throw new CannotJoinException(l6e(player.getLocale(), "duel.matchrun"));
		if (getPlayers().size() >= 4)
			throw new CannotJoinException(l6e(player.getLocale(), "duel.teamcompleted"));
		super.joinMatch(player);
		player.teleport(arena.getArenaPoint("waiting_room"));
		getPlayers().forEach(p -> p.sendMessage(ChatColor.GOLD + player.getName() + ChatColor.RESET + " " + l6e(player.getLocale(), "duel.entered")));
		ItemStack is = new ItemStack(Material.WOOD_SWORD);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(l6e(player.getLocale(), "duel.selectkit"));
		im.setLore(Collections.singletonList("forkits"));
		is.setItemMeta(im);
		player.getInventory().addItem(is);
		openGuiСhoice(player);
	}

	@Override
	public void stateTransition(IMatchState targetState) {
		super.stateTransition(targetState);
		if (targetState == MatchRunState.RUNNING) {
			getPlayers().forEach(p -> {
				p.getInventory().clear();
				try {
					KitWrapperProvider.INSTANCE.wrap(selectedkits.get(p)).give(p);
				} catch (Exception e) {//default
					MiniGameInventoryPlayerItem pi = new MiniGameInventoryPlayerItem();
					pi.owner = p.getUniqueId();
					pi.type = "Kit";
					pi.minigame = "duel";
					pi.name = "kit1";
					KitWrapperProvider.INSTANCE.wrap(pi).give(p);
				}
			});
			getPlayers(DuelMatch.DuelTeam.A).forEach(p -> p.teleport(arena.getArenaPoint("team_a")));
			getPlayers(DuelMatch.DuelTeam.B).forEach(p -> p.teleport(arena.getArenaPoint("team_b")));
		}
		if (targetState == MatchRunState.FINISHED) {
			getPlayers().forEach(p -> leaveMatch(p, null));
		}
	}

	@Override
	public Set<ITeam> getTeams() {
		return ImmutableSet.of(UnknownTeam.INSTANCE, DuelMatch.DuelTeam.A, DuelMatch.DuelTeam.B);
	}

	@Override
	public void leaveMatch(Player player, Iterator<Player> it) {
		ScoreBoard.removeScoreBoardFromPlayer(player);
		super.leaveMatch(player, it);
		if (getPlayers().size() == 1 && state == MatchRunState.RUNNING) {
			DuelMatch.failerPlayers(new ArrayList<>(Collections.singleton(player.getUniqueId())));
			finishCooldown.execute();
		}
	}

	@Override
	public void update() {
		ItemMeta imeta = selectableBlocks.get(0).getItemMeta();
		imeta.setDisplayName(String.format("Team A (%s/%s)", getPlayers(DuelMatch.DuelTeam.A).size(), 2));
		selectableBlocks.get(0).setItemMeta(imeta);

		imeta = selectableBlocks.get(1).getItemMeta();
		imeta.setDisplayName(String.format("Team B (%s/%s)", getPlayers(DuelMatch.DuelTeam.B).size(), 2));
		selectableBlocks.get(1).setItemMeta(imeta);
		DuelMatch.renameKits();
		updateScoreBoard();
		startCooldown.update();
		finishCooldown.update();
		autoselectTeams();
		super.update();
	}

	@Override
	public ArrayList getLore() {
		ArrayList<String> ret = new ArrayList<>();
		if (lockKit != -1) ret.add("Залоченный кит: " + DuelMatch.renameItems(lockKit));
		return ret;
	}
	@Override
	public void onOpenGui(Player player, String s) throws NullPointerException {
		if (state == MatchRunState.PREPARING) {
			if (s.equals("ch") || s.equals("team") || s.equals("teams") || s.equals("t") || s.equals(""))
				openGuiСhoice(player);
			else throw new NullPointerException();
		} else {
			player.sendMessage(l6e(player.getLocale(), "duel.notallowed"));
		}
	}

	public void openGuiСhoice(Player pl) {
		InventoryGui gui = new InventoryGui(Duel.instance, ChatColor.BLUE + l6e(pl.getLocale(), "duel.selectkit"), blockSelectGuiLayout);
		GuiElementGroup group = new GuiElementGroup('g');

		group.setFiller(new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.MAGENTA.getWoolData()));
		if (lockKit != -1) {
			GuiElementGroup groupkit = new GuiElementGroup('k');
			for (ItemStack stack : DuelMatch.selectableKits) {
				groupkit.addElement(new StaticGuiElement('k', stack, new BlockSelectAction(selectableBlocks, selectedkits, this)));
			}
			gui.addElement(groupkit);
		}
		GuiElementGroup groupblock = new GuiElementGroup('b');
		for (ItemStack stack : selectableBlocks) {
			groupblock.addElement(new StaticGuiElement('b', stack, new BlockSelectAction(selectableBlocks, selectedkits, this)));
		}

		gui.addElement(group);
		gui.addElement(groupblock);

		gui.show(pl);
	}

	public void updateScoreBoard() {
		getPlayers().forEach(p -> {
			JsonObject jo = new JsonObject();
			jo.addProperty(l6e(p.getLocale(), "duel.arenatype"), getArena().getConfig().getString("arenatype"));
			if (Duel.DEBUG) jo.addProperty("Arena name", arena.getName());
			if (Duel.DEBUG) jo.addProperty("State", state + "");
			jo.addProperty(l6e(p.getLocale(), "duel.team"), DuelMatch.getTeamString(p, getPlayerTeam(p)));
			jo.addProperty(l6e(p.getLocale(), "duel.players"), getPlayers().size() + "/4");
			if (lockKit != -1) jo.addProperty(l6e(p.getLocale(), "duel.lockkit"), DuelMatch.renameItems(lockKit));
			if (selectedkits.containsKey(p))
				jo.addProperty(l6e(p.getLocale(), "duel.selkit"), selectedkits.get(p).toString());
			if (Duel.DEBUG) {
				JsonArray ja = new JsonArray();
				ja.add("Z: " + p.getLocation().getBlockZ());
				ja.add("Y: " + p.getLocation().getBlockY());
				ja.add("X: " + p.getLocation().getBlockX());
				jo.add("", ja);
			}
			ScoreBoard.updateSB(p,
					ScoreBoard.preparationJson(jo, "Duel", "sb"));
		});
	}

	public void autoselectTeams() {
		getPlayers().forEach(p -> {
			try {
				if (getPlayers(DuelMatch.DuelTeam.A).size() <= 1 && arena.getCurrentMatch().getPlayerTeam(p).equals(UnknownTeam.INSTANCE))
					joinTeam(p, DuelMatch.DuelTeam.A, false);
				else if (getPlayers(DuelMatch.DuelTeam.B).size() <= 1 && arena.getCurrentMatch().getPlayerTeam(p).equals(UnknownTeam.INSTANCE))
					joinTeam(p, DuelMatch.DuelTeam.B, false);
				else if (arena.getCurrentMatch().getPlayerTeam(p) != null)
					return;
				else
					p.sendMessage("Что ты тут сука забыл? =\\");

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
}
