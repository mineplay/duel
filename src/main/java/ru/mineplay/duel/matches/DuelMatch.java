package ru.mineplay.duel.matches;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.mineplay.duel.Duel;
import ru.mineplay.duel.DuelDBManager;
import ru.mineplay.duel.Vault;
import ru.mineplay.duel.games.DuelMiniGame;
import ru.mineplay.duel.utils.BlockSelectAction;
import ru.mineplay.kits.KitWrapperProvider;
import ru.mineplay.mineplayapi.bars.ScoreBoard;
import ru.mineplay.mineplayapi.game.arena.Arena;
import ru.mineplay.mineplayapi.game.cooldown.CooldownManager;
import ru.mineplay.mineplayapi.game.match.CannotJoinException;
import ru.mineplay.mineplayapi.game.match.IMatchState;
import ru.mineplay.mineplayapi.game.match.MatchBase;
import ru.mineplay.mineplayapi.game.match.MatchRunState;
import ru.mineplay.mineplayapi.game.stat.BasicMiniGameStats;
import ru.mineplay.mineplayapi.game.stat.BasicMiniGameStats.Template;
import ru.mineplay.mineplayapi.game.team.ITeam;
import ru.mineplay.mineplayapi.game.team.UnknownTeam;
import ru.mineplay.mineplayapi.inventory.item.MiniGameInventoryPlayerItem;
import ru.mineplay.mineplayapi.invgui.GuiElementGroup;
import ru.mineplay.mineplayapi.invgui.InventoryGui;
import ru.mineplay.mineplayapi.invgui.StaticGuiElement;
import ru.mineplay.mineplayapi.pojo.PersistentPlayerStats;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static ru.mineplay.mineplayapi.i18n.MPLocalizer.l6e;

public class DuelMatch extends MatchBase<DuelMiniGame> {
	public int winCoin = 50;
	public int lockKit = -1;
	/**
	 * ~1x1~
	 */
	private HashMap<Player, MiniGameInventoryPlayerItem> selectedkits = new HashMap<>();
	public static final String[] blockSelectGuiLayout = new String[]{
			"bbggggggg",
			"ggggggggg",
			"kkkkkkkkk"
	};

	public static ArrayList<ItemStack> selectableKits = Lists.newArrayList(
			new ItemStack(Material.IRON_SWORD),
			new ItemStack(Material.POTION),
			new ItemStack(Material.STONE_SWORD),
			new ItemStack(Material.DIAMOND_CHESTPLATE),
			new ItemStack(Material.BOW),
			new ItemStack(Material.FISHING_ROD),
			new ItemStack(Material.DIAMOND_SWORD),
			new ItemStack(Material.FLINT_AND_STEEL),
			new ItemStack(Material.TNT));
	public CooldownManager<DuelMatch> startCooldown = new CooldownManager<DuelMatch>(100) {
		@Override
		public boolean checkConditions() {
			return getRunState() == MatchRunState.PREPARING && getPlayers().size() == 2;
		}

		@Override
		public void execute() {
			stateTransition(MatchRunState.RUNNING);
		}
	}.setCooldownDisplayHandler(new CooldownManager.TitleCooldownDisplayHandler(this,
			20, "Игра начнётся через %d секунд",
			ImmutableSet.of(DuelTeam.A, DuelTeam.B)), 20, 100);

	public ArrayList<ItemStack> selectableBlocks = Lists.newArrayList(
			new ItemStack(Material.REDSTONE_BLOCK),
			new ItemStack(Material.GLOWSTONE));

	public DuelMatch(DuelMiniGame minigame, Arena<DuelMiniGame> arena) {
		super(minigame, arena);
		initConfig();
	}

	@Override
	protected Template createTeamStatsTemplate() {
		return new BasicMiniGameStats.BasicTemplate();
	}

	@Override
	protected Template createPlayerStatsTemplate() {
		return new BasicMiniGameStats.BasicTemplate();
	}

	public CooldownManager<DuelMatch> finishCooldown = new CooldownManager<DuelMatch>(3600) {
		@Override
		public boolean checkConditions() {
			return getRunState() == MatchRunState.RUNNING;
		}

		@Override
		public void execute() {

			if (getPlayers(DuelTeam.A).size() == 0)
				winTeam(getPlayers(DuelTeam.B).stream().map(Player::getUniqueId).collect(Collectors.toList()), winCoin);
			if (getPlayers(DuelTeam.B).size() == 0)
				winTeam(getPlayers(DuelTeam.A).stream().map(Player::getUniqueId).collect(Collectors.toList()), winCoin);
			if (getPlayers(DuelTeam.A).size() > 0 && getPlayers(DuelTeam.B).size() > 0) {
				List<UUID> list = getPlayers(DuelTeam.A).stream().map(Player::getUniqueId).collect(Collectors.toList());
				list.addAll(getPlayers(DuelTeam.B).stream().map(Player::getUniqueId).collect(Collectors.toList()));
				failerPlayers(list);
			}
			stateTransition(MatchRunState.FINISHED);
		}
	}.setCooldownDisplayHandler(
			new CooldownManager.ActionBarCooldownDisplayHandler(this,
					20, "Игра окончится через %d секунд",
					ImmutableSet.of(DuelTeam.A, DuelTeam.B)), 20, -1);

	@Override
	public void stateTransition(IMatchState targetState) {
		super.stateTransition(targetState);
		if (targetState == MatchRunState.RUNNING) {
			getPlayers().forEach(p -> {
				p.getInventory().clear();
				try {
					KitWrapperProvider.INSTANCE.wrap(selectedkits.get(p)).give(p);
				} catch (Exception e) {//default
					MiniGameInventoryPlayerItem pi = new MiniGameInventoryPlayerItem();
					pi.owner = p.getUniqueId();
					pi.type = "Kit";
					pi.minigame = "duel";
					pi.name = "kit1";
					KitWrapperProvider.INSTANCE.wrap(pi).give(p);
				}
			});
			getPlayers(DuelTeam.A).forEach(p -> p.teleport(arena.getArenaPoint("team_a")));
			getPlayers(DuelTeam.B).forEach(p -> p.teleport(arena.getArenaPoint("team_b")));
		}
		if (targetState == MatchRunState.FINISHED) {
			getPlayers().forEach(p -> leaveMatch(p, null));
		}
	}

	@Override
	public int getMaxPlayers() {
		return 2;
	}
	@Override
	public Set<ITeam> getTeams() {
		return ImmutableSet.of(UnknownTeam.INSTANCE, DuelTeam.A, DuelTeam.B);
	}

	public static void renameKits() {
		ArrayList<String> kitSets = new ArrayList<>();
		kitSets.add("Железный меч /n" +
				"Лук /n" +
				"10 стрел /n" +
				"Зажигалка(10 использований)");
		kitSets.add("Алмазный меч(острота 5) /n" +
				"Зелье скорости(1:30) /n" +
				"Зелье здоровья(весь инвентарь) /n" +
				"Алмазная броня(полный сет, защита 2)");
		kitSets.add("Каменый меч /n" +
				"Грибной суп(весь инвентарь) /n" +
				"Алмазная броня");
		kitSets.add("Алмазка(защита 5) /n" +
				"Алмазный меч(острота 5) /n" +
				"Удочка /n" +
				"Лук(сила 4) /n" +
				"30 стрел /n" +
				"Зажигалка(15 использований) /n" +
				"Зелье скорости(0:45) /n" +
				"Зелье регенерации(0:10) /n" +
				"8 золотых яблок");
		kitSets.add("Лук(бесконечность) /n" +
				"1 стрела /n" +
				"Кожанная броня");
		kitSets.add("Алмазный меч(острота 3) /n" +
				"Лук(сила 2) /n" +
				"Удочка /n" +
				"6 золотых яблок /n" +
				"2 стака деревянных блоков /n" +
				"Топор /n" +
				"2 ведра лавы /n" +
				"2 ведра воды /n" +
				"Алмазная броня(защита 2)");
		kitSets.add("Алмазная броня /n" +
				"Алмазный меч /n" +
				"Лук /n" +
				"25 стрел /n" +
				"128 песчаника");
		kitSets.add("Алмазная броня(защита 3) /n" +
				"Алмазный меч(заговор огня 2) /n" +
				"Лук(огненая стрела) /n" +
				"20 стрел /n" +
				"Зажигалка(20 использований) /n" +
				"Удочка /n" +
				"6 золотых яблок /n" +
				"3 ведра лавы /n" +
				"3 ведра воды /n");
		kitSets.add("Алмазная броня(защита 4) /n" +
				"Алмазный меч(острота 1,отдача 1) /n" +
				"8 ТНТ /n" +
				"Зажигалка /n" +
				"4 золотых яблока /n" +
				"Удочка");
		for (ItemStack k : selectableKits) {
			ItemMeta im = k.getItemMeta();
			im.setDisplayName(renameItems(selectableKits.indexOf(k) + 1));
			im.setLore(Arrays.asList(kitSets.get(selectableKits.indexOf(k)).split("/n")));
			k.setItemMeta(im);
		}
	}

	@Override
	public void joinMatch(Player player) throws CannotJoinException {
		if (getRunState().getBaseState() != MatchRunState.PREPARING)
			throw new CannotJoinException(l6e(player.getLocale(), "duel.matchrun"));
		if (getPlayers().size() >= 2)
			throw new CannotJoinException(l6e(player.getLocale(), "duel.teamcompleted"));
		super.joinMatch(player);

		player.teleport(arena.getArenaPoint("waiting_room"));
		getPlayers().forEach(p -> p.sendMessage(ChatColor.GOLD + player.getName() + ChatColor.RESET + " " + l6e(player.getLocale(), "duel.entered")));
		ItemStack is = new ItemStack(Material.WOOD_SWORD);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(l6e(player.getLocale(), "duel.selectkit"));
		is.setItemMeta(im);
		im.setLore(Collections.singletonList("forkits"));
		player.getInventory().setItem(8, is);
		openGuiСhoice(player);
	}

	public static String renameItems(int i) {
		switch (i) {
			case 1:
				return "Классический";
			case 2:
				return "Зелья";
			case 3:
				return "Старый";
			case 4:
				return "OP";
			case 5:
				return "На луках";
			case 6:
				return "Набор UHC";
			case 7:
				return "Набор BedWars";
			case 8:
				return "Огненный";
			case 9:
				return "Набор TNT";
			default:
				return "Unknown";
		}
	}

	@Override
	public ArrayList getLore() {
		ArrayList<String> ret = new ArrayList<>();
		if (lockKit != -1) ret.add("Залоченный кит: " + renameItems(lockKit));
		return ret;
	}

	public static void failerPlayers(List<UUID> teamlist) {
		Bukkit.getScheduler().runTaskAsynchronously(Duel.instance, () -> {
			try {
				Dao<PersistentPlayerStats, UUID> dao = DuelDBManager.getPersistentPlayerStats();
				for (UUID uuid : teamlist) {
					PersistentPlayerStats playerStats = dao.queryForId(uuid);
					if (playerStats == null) {
						playerStats = new PersistentPlayerStats();
						playerStats.id = uuid;
					}
					playerStats.fails++;
					PersistentPlayerStats.updateRating(playerStats);
					dao.createOrUpdate(playerStats);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public static void winTeam(Collection<UUID> winnerUUIDs, double winCoin) {
		Bukkit.getScheduler().runTaskAsynchronously(Duel.instance, () -> {
			try {
				Dao<PersistentPlayerStats, UUID> dao = DuelDBManager.getPersistentPlayerStats();
				for (UUID uuid : winnerUUIDs) {
					PersistentPlayerStats playerStats = dao.queryForId(uuid);
					if (playerStats == null) {
						playerStats = new PersistentPlayerStats();
						playerStats.id = uuid;

					}
					playerStats.wins++;
					PersistentPlayerStats.updateRating(playerStats);
					dao.createOrUpdate(playerStats);
					Player pl = Bukkit.getPlayer(uuid);
					int mult = 1;
					if (pl.hasPermission("bonus.2x"))
						mult = 2;
					else if (pl.hasPermission("bonus.3x"))
						mult = 3;
					Vault.addMoney(pl, winCoin * mult);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void leaveMatch(Player player, Iterator<Player> it) {
		ScoreBoard.removeScoreBoardFromPlayer(player);
		super.leaveMatch(player, it);
		if (getPlayers().size() == 1 && state == MatchRunState.RUNNING) {
			failerPlayers(new ArrayList<>(Collections.singleton(player.getUniqueId())));
			finishCooldown.execute();
		}
	}

	private void initConfig() {
		if (getArena().getConfig().contains("wincoin"))
			winCoin = getArena().getConfig().getInt("wincoin");
		if (getArena().getConfig().contains("lockkit"))
			lockKit = getArena().getConfig().getInt("lockkit");
	}

	@Override
	public void update() {
		ItemMeta imeta = selectableBlocks.get(0).getItemMeta();
		imeta.setDisplayName(String.format("Team A (%s/%s)", getPlayers(DuelTeam.A).size(), 1));
		selectableBlocks.get(0).setItemMeta(imeta);

		imeta = selectableBlocks.get(1).getItemMeta();
		imeta.setDisplayName(String.format("Team B (%s/%s)", getPlayers(DuelTeam.B).size(), 1));
		selectableBlocks.get(1).setItemMeta(imeta);
		renameKits();
		updateScoreBoard();
		startCooldown.update();
		finishCooldown.update();
		autoselectTeams();

		super.update();
	}

	public static String getTeamString(Player p, ITeam team) {
		if (team.equals(DuelMatch.DuelTeam.A))
			return ChatColor.RED + l6e(p.getLocale(), "duel.teama");
		else if (team.equals(DuelMatch.DuelTeam.B))
			return ChatColor.BLUE + l6e(p.getLocale(), "duel.teamb");
		else return ChatColor.GOLD + l6e(p.getLocale(), "duel.teamwait");
	}

	public void updateScoreBoard() {
		getPlayers().forEach(p -> {
			JsonObject jo = new JsonObject();
			jo.addProperty(l6e(p.getLocale(), "duel.arenatype"), getArena().getConfig().getString("arenatype"));
			if (Duel.DEBUG) jo.addProperty("Arena name", arena.getName());
			if (Duel.DEBUG) jo.addProperty("State", state + "");
			jo.addProperty(l6e(p.getLocale(), "duel.team"), getTeamString(p, getPlayerTeam(p)));
			jo.addProperty(l6e(p.getLocale(), "duel.players"), getPlayers().size() + "/2");
			if (lockKit != -1) jo.addProperty(l6e(p.getLocale(), "duel.lockkit"), renameItems(lockKit));
			if (selectedkits.containsKey(p))
				jo.addProperty(l6e(p.getLocale(), "duel.selkit"), selectedkits.get(p).toString());
			if (Duel.DEBUG) {
				JsonArray ja = new JsonArray();
				ja.add("Z: " + p.getLocation().getBlockZ());
				ja.add("Y: " + p.getLocation().getBlockY());
				ja.add("X: " + p.getLocation().getBlockX());
				jo.add("", ja);
			}
			ScoreBoard.updateSB(p,
					ScoreBoard.preparationJson(jo, "Duel", "sb"));
		});
	}

	public void autoselectTeams() {
		getPlayers().forEach(p -> {
			try {
				if (getPlayers(DuelTeam.A).size() == 0 && arena.getCurrentMatch().getPlayerTeam(p).equals(UnknownTeam.INSTANCE))
					joinTeam(p, DuelTeam.A, false);
				else if (getPlayers(DuelTeam.B).size() == 0 && arena.getCurrentMatch().getPlayerTeam(p).equals(UnknownTeam.INSTANCE))
					joinTeam(p, DuelTeam.B, false);
				else if (arena.getCurrentMatch().getPlayerTeam(p) != null)
					return;
				else
					p.sendMessage("Что ты тут сука забыл? =\\");

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

	}

	@Override
	public void onOpenGui(Player player, String s) throws NullPointerException {
		if (state == MatchRunState.PREPARING) {
			if (s.equals("ch") || s.equals("team") || s.equals("teams") || s.equals("t") || s.equals(""))
				openGuiСhoice(player);
			else throw new NullPointerException();
		} else {
			player.sendMessage(l6e(player.getLocale(), "duel.notallowed"));
		}
	}

	public void openGuiСhoice(Player pl) {
		InventoryGui gui = new InventoryGui(Duel.instance, ChatColor.BLUE + l6e(pl.getLocale(), "duel.selectkit"), blockSelectGuiLayout);
		GuiElementGroup group = new GuiElementGroup('g');

		group.setFiller(new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.MAGENTA.getWoolData()));
		if (lockKit != -1) {
			GuiElementGroup groupkit = new GuiElementGroup('k');
			for (ItemStack stack : selectableKits) {
				groupkit.addElement(new StaticGuiElement('k', stack, new BlockSelectAction(selectableBlocks, selectedkits, this)));
			}
			gui.addElement(groupkit);
		}
		GuiElementGroup groupblock = new GuiElementGroup('b');
		for (ItemStack stack : selectableBlocks) {
			groupblock.addElement(new StaticGuiElement('b', stack, new BlockSelectAction(selectableBlocks, selectedkits, this)));
		}

		gui.addElement(group);
		gui.addElement(groupblock);

		gui.show(pl);
	}

	public enum DuelTeam implements ITeam {
		A("teamA", "Команда A"), B("teamB", "Команда Б");

		public String id;
		public String title;

		DuelTeam(String id, String title) {
			this.id = id;
			this.title = title;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getTitle() {
			return title;
		}

		@Override
		public boolean shouldShowInTables() {
			return true;
		}
	}
}
