package ru.mineplay.duel;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import org.bukkit.plugin.Plugin;

public class DuelPacketListener implements PacketListener {
	@Override
	public void onPacketSending(PacketEvent packetEvent) {

	}

	@Override
	public void onPacketReceiving(PacketEvent packetEvent) {
		/*if (packetEvent.getPacket().getType() == PacketType.Play.Client.BLOCK_DIG)
			packetEvent.setCancelled(true);
		if(packetEvent.getPacket().getType() == PacketType.Play.Client.BLOCK_PLACE)
			packetEvent.setCancelled(true);
		if(packetEvent.getPacket().getType() == PacketType.Play.Server.BLOCK_ACTION)
			packetEvent.setCancelled(true);*/
	}

	@Override
	public ListeningWhitelist getSendingWhitelist() {
		return ListeningWhitelist.EMPTY_WHITELIST;
	}

	@Override
	public ListeningWhitelist getReceivingWhitelist() {
		return ListeningWhitelist.newBuilder()
				.gamePhase(GamePhase.PLAYING)
				.types(PacketType.Play.Client.BLOCK_DIG, PacketType.Play.Client.USE_ENTITY)
				.build();

	}

	@Override
	public Plugin getPlugin() {
		return Duel.instance;
	}
}
