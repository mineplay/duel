package ru.mineplay.duel;

import com.j256.ormlite.dao.Dao;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.mineplay.duel.games.DuelMiniGame;
import ru.mineplay.duel.matches.DuelMatch;
import ru.mineplay.duel.matches.DuelMatchTeamNew;
import ru.mineplay.mineplayapi.game.match.Match;
import ru.mineplay.mineplayapi.game.match.MatchBase;
import ru.mineplay.mineplayapi.pojo.PersistentPlayerStats;

import java.sql.SQLException;
import java.util.UUID;

public class DuelEvents implements Listener {
	/*@EventHandler
	public void disablePlace(BlockPlaceEvent event) {
		if (!event.getPlayer().isOp())
			event.setCancelled(true);
	}

	@EventHandler
	public void disableBreak(BlockBreakEvent event) {
		if (!event.getPlayer().isOp())
			event.setCancelled(true);
	}

	@EventHandler
	public void disableBreak(PlayerInteractEvent event) {
		if (!event.getPlayer().isOp())
			event.setCancelled(true);
	}*/

	@EventHandler
	public void disableInteract(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			Player player = event.getPlayer();
			Match<?> match = Match.getMatch(player);
			try {
				if (match != null
						&& player.getInventory().getItemInMainHand().getItemMeta().hasLore()
						&& player.getInventory().getItemInMainHand().getItemMeta().getLore().contains("forkits")
						&& match instanceof MatchBase)
					match.onOpenGui(player, "");
			} catch (Exception ignore) {
			}
		}
	}
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		Player player = (Player) event.getEntity();
		Match<?> match_ = Match.getMatch(player);
		if (match_ != null) {
			if (match_ instanceof DuelMatch)
				duelmatch((DuelMatch) match_, player);
			if (match_ instanceof DuelMatchTeamNew) {
				duelmatch((DuelMatchTeamNew) match_, player);
			}
/*			else if (match_ instanceof DuelMatchFour)
				duelmatch((DuelMatchFour) match_, player);*/
		}
	}

	private void duelmatch(MatchBase<? extends DuelMiniGame> match, Player player) {
		UUID uuid = player.getUniqueId();
		Player killer = player.getKiller();
		UUID killerUUID = killer == null ? null : killer.getUniqueId();

		Bukkit.getScheduler().runTaskAsynchronously(Duel.instance, () -> {
			try {
				Dao<PersistentPlayerStats, UUID> dao = DuelDBManager.getPersistentPlayerStats();
				PersistentPlayerStats playerStats = dao.queryForId(uuid);
				if (playerStats == null) {
					playerStats = new PersistentPlayerStats();
					playerStats.id = player.getUniqueId();
				}
				playerStats.deaths++;
				PersistentPlayerStats.updateRating(playerStats);
				dao.createOrUpdate(playerStats);
				if (killerUUID != null) {
					PersistentPlayerStats killerStats = dao.queryForId(killerUUID);
					if (killerStats == null) {
						killerStats = new PersistentPlayerStats();
						killerStats.id = player.getUniqueId();
					}
					killerStats.kills++;
					PersistentPlayerStats.updateRating(killerStats);
					dao.createOrUpdate(killerStats);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
		match.leaveMatch(player, null);
	}
}
