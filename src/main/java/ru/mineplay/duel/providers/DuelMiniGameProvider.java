package ru.mineplay.duel.providers;

import org.bukkit.configuration.ConfigurationSection;
import ru.mineplay.duel.games.DuelMiniGame;
import ru.mineplay.mineplayapi.game.MiniGameProvider;

public class DuelMiniGameProvider implements MiniGameProvider<DuelMiniGame> {
	@Override
	public String getName() {
		return "Duel";
	}

	@Override
	public DuelMiniGame newInstance(ConfigurationSection configurationSection) {
		return new DuelMiniGame(configurationSection);
	}
}
