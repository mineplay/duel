package ru.mineplay.duel.utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.mineplay.duel.games.DuelMiniGame;
import ru.mineplay.duel.matches.DuelMatch;
import ru.mineplay.mineplayapi.game.match.MatchBase;
import ru.mineplay.mineplayapi.inventory.item.MiniGameInventoryPlayerItem;
import ru.mineplay.mineplayapi.invgui.GuiElement;

import java.util.ArrayList;
import java.util.HashMap;

import static ru.mineplay.mineplayapi.i18n.MPLocalizer.l6e;

public class BlockSelectAction implements GuiElement.Action {
	ArrayList<ItemStack> selectableBlocks;
	MatchBase<DuelMiniGame> match;
	HashMap<Player, MiniGameInventoryPlayerItem> selectedkits;

	public BlockSelectAction(ArrayList<ItemStack> selblock, HashMap<Player, MiniGameInventoryPlayerItem> selkit, MatchBase<DuelMiniGame> match) {
		this.selectableBlocks = selblock;
		this.match = match;
		this.selectedkits = selkit;
	}

	@Override
	public boolean onClick(GuiElement.Click click) {
		if (!(click.getEvent().getWhoClicked() instanceof Player))
			return false;

		ItemStack stack = click.getElement().getItem(click.getSlot());
		Player player = (Player) click.getEvent().getWhoClicked();
		try {
			if (selectableBlocks.get(0).isSimilar(stack) && match.getPlayers(DuelMatch.DuelTeam.A).size() <= 1) {
				match.joinTeam(player, DuelMatch.DuelTeam.A, false);
				player.sendMessage(l6e(player.getLocale(), "duel.youentered") + ChatColor.RED + l6e(player.getLocale(), "duel.teama"));
			}
			if (selectableBlocks.get(1).isSimilar(stack) && match.getPlayers(DuelMatch.DuelTeam.B).size() <= 1) {
				match.joinTeam(player, DuelMatch.DuelTeam.B, false);
				player.sendMessage(l6e(player.getLocale(), "duel.youentered") + ChatColor.BLUE + l6e(player.getLocale(), "duel.teamb"));
			}

			for (ItemStack is : DuelMatch.selectableKits) {
				if (is.isSimilar(stack)) {
					MiniGameInventoryPlayerItem pi = new MiniGameInventoryPlayerItem();
					pi.owner = player.getUniqueId();
					pi.type = "Kit";
					pi.minigame = "duel";
					pi.name = "kit" + DuelMatch.selectableKits.indexOf(is);

					selectedkits.put(player, pi);
					player.sendMessage(l6e(player.getLocale(), "duel.youselectedkit", is.getItemMeta().getDisplayName()));
				}
			}
		} catch (Exception ignore) {
		}
		player.closeInventory();
		return true;
	}
}
