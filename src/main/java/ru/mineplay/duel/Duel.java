package ru.mineplay.duel;

import com.comphenix.protocol.ProtocolLibrary;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import ru.mineplay.duel.providers.DuelMiniGameProvider;
import ru.mineplay.mineplayapi.game.MiniGameProvider;
import ru.mineplay.mineplayapi.i18n.MPLocalizer;

public final class Duel extends JavaPlugin {
	public static Duel instance;
	public static boolean DEBUG = false;
	private DuelPacketListener DuelPacketListener = new DuelPacketListener();
	public static Economy econ;

	public static Plugin getPlugin() {
		return instance;
	}

	@Override
	public void onEnable() {
		super.onEnable();
		instance = this;
		MPLocalizer.loadLocaleFiles(this, "duel");
		this.getServer().getPluginManager().registerEvents(new DuelEvents(), this);

		ProtocolLibrary.getProtocolManager().addPacketListener(DuelPacketListener);

		getServer().getServicesManager()
				.register(MiniGameProvider.class, new DuelMiniGameProvider(), this, ServicePriority.Normal);
		Vault.setupEconomy();
	}


	@Override
	public void onDisable() {
		super.onDisable();
		ProtocolLibrary.getProtocolManager().removePacketListener(DuelPacketListener);
	}
}
